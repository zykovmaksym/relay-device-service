package max.smarthome.relayservice.controller

import max.smarthome.relayservice.controller.dto.RelayClick
import max.smarthome.relayservice.controller.dto.RelayOnClickCallback
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.web.bind.annotation.RestController


@RestController
class RelayController(
        private val messagingTemplate: SimpMessagingTemplate,
) {
    @MessageMapping("/on-click")
    fun onClick(@Payload relayClick: RelayClick) {
        println("relayClick: $relayClick")
        messagingTemplate.convertAndSendToUser(relayClick.deviceId.toString(), "/on-click", relayClick)
    }

    @MessageMapping("/on-click/callback")
    fun callback(@Payload relayOnClickCallback: RelayOnClickCallback) {
        println("relayOnClickCallback: $relayOnClickCallback")
        messagingTemplate.convertAndSendToUser(relayOnClickCallback.deviceId.toString(), "/on-click/callback", relayOnClickCallback)
    }
}