package max.smarthome.relayservice.controller.dto

import java.util.*

data class RelayClick(
        val deviceId: UUID
)
