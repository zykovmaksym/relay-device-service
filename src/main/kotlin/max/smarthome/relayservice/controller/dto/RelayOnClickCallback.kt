package max.smarthome.relayservice.controller.dto

import max.smarthome.relayservice.common.DeviceId

data class RelayOnClickCallback(
        val type: String,
        val deviceId: DeviceId,
        val status: Boolean
)
