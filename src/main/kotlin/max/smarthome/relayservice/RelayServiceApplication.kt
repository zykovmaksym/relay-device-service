package max.smarthome.relayservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RelayServiceApplication

fun main(args: Array<String>) {
    runApplication<RelayServiceApplication>(*args)
}
