package max.smarthome.relayservice.rest.repository

import max.smarthome.relayservice.common.DeviceId
import max.smarthome.relayservice.rest.model.GetLastRelay
import max.smarthome.relayservice.rest.model.Relay
import max.smarthome.relayservice.rest.model.InsertRelay
import org.springframework.stereotype.Repository
import java.time.LocalDateTime
import java.util.*

@Repository
class RelayRepositoryImpl: RelayRepository {
    private var relayList = listOf(
            Relay(
                    deviceId = UUID.randomUUID(),
                    status = false,
                    dateTime = LocalDateTime.now(),
                    "a4-e5-7c-01-26-9e"
            )
    )

    override fun getLastRelay(mac: String): GetLastRelay = GetLastRelay(relayList.last { it.mac == mac }.status)

    override fun insertRelay(mac: String, insertRelay: InsertRelay): DeviceId {
        val relay = Relay(
                deviceId = UUID.randomUUID(),
                status = insertRelay.status,
                dateTime = LocalDateTime.now(),
                mac = mac
        )
        relayList = relayList + relay
        return relay.deviceId
    }
}