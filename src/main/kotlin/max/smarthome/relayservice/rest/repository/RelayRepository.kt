package max.smarthome.relayservice.rest.repository

import max.smarthome.relayservice.common.DeviceId
import max.smarthome.relayservice.rest.model.GetLastRelay
import max.smarthome.relayservice.rest.model.InsertRelay

interface RelayRepository {
    fun getLastRelay(mac: String): GetLastRelay
    fun insertRelay(mac: String, insertRelay: InsertRelay): DeviceId
}