package max.smarthome.relayservice.rest.service

import max.smarthome.relayservice.common.DeviceId
import max.smarthome.relayservice.rest.model.InsertRelay
import max.smarthome.relayservice.rest.repository.RelayRepository
import org.springframework.stereotype.Component

@Component
class InsertRelayUseCase(
        private val relayRepository: RelayRepository
) {
    operator fun invoke(mac: String, insertRelay: InsertRelay): DeviceId = relayRepository.insertRelay(mac, insertRelay)
}