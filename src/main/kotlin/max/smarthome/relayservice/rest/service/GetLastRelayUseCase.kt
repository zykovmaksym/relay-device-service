package max.smarthome.relayservice.rest.service

import max.smarthome.relayservice.rest.model.GetLastRelay
import max.smarthome.relayservice.rest.repository.RelayRepository
import org.springframework.stereotype.Component

@Component
class GetLastRelayUseCase(
        private val relayRepository: RelayRepository
) {
    operator fun invoke(mac: String): GetLastRelay =
            relayRepository.getLastRelay(mac)
}