package max.smarthome.relayservice.rest.service

import org.springframework.stereotype.Service

@Service
class RelayService(
        val getLastRelayUseCase: GetLastRelayUseCase,
        val insertRelayUseCase: InsertRelayUseCase
)