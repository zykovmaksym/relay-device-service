package max.smarthome.relayservice.rest.model

data class GetLastRelay(
        val status: Boolean,
)
