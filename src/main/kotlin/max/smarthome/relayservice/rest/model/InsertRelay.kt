package max.smarthome.relayservice.rest.model

data class InsertRelay(
        val status: Boolean
)