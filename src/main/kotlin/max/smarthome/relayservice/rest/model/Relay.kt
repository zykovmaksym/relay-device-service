package max.smarthome.relayservice.rest.model

import max.smarthome.relayservice.common.DeviceId
import java.time.LocalDateTime

data class Relay(
        val deviceId: DeviceId,
        val status: Boolean,
        val dateTime: LocalDateTime,
        val mac: String
)
