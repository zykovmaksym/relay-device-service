package max.smarthome.relayservice.rest.controller

import max.smarthome.relayservice.common.DeviceId
import max.smarthome.relayservice.rest.model.InsertRelay
import max.smarthome.relayservice.rest.service.RelayService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/client/relay")
class RelayClientController(
        private val relayService: RelayService
) {
    @PostMapping("/{mac}")
    fun addRelay(@PathVariable mac: String, @RequestBody relay: InsertRelay): DeviceId {
        val relayId = relayService.insertRelayUseCase(mac, relay)
        println(relayId)
        return relayId
    }
}