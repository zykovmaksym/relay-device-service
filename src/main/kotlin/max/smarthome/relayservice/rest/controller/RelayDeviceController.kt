package max.smarthome.relayservice.rest.controller

import max.smarthome.relayservice.rest.model.GetLastRelay
import max.smarthome.relayservice.rest.service.RelayService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/device/relay/{mac}")
class RelayDeviceController(
        private val relayService: RelayService
) {
    @GetMapping
    fun fetchLastRelay(@PathVariable mac: String): ResponseEntity<GetLastRelay> {
        val relay = relayService.getLastRelayUseCase(mac)
        println(relay)
        return ResponseEntity(relay, HttpStatus.OK)
    }
}